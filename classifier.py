import numpy as np 
import csv
from datetime import datetime
import time

def loadDataFiles():
	behavior_data = []
	user_data = []
	cast_data = []
	video_data = []
	with open("Rakuten-Viki-Data//Behavior_training.csv", 'r') as f:
		next(f)                  		# Skip first line
		for row in csv.reader(f):
			behavior_data.append(row)
		behavior_data = np.array(behavior_data)
	with open("Rakuten-Viki-Data//User_attributes.csv", 'r') as f:
		next(f)
		for row in csv.reader(f):
			user_data.append(row)
		user_data = np.array(user_data)
	with open("Rakuten-Viki-Data//Video_casts.csv", 'r') as f:
		next(f)
		for row in csv.reader(f):
			cast_data.append(row)
		cast_data = np.array(cast_data)
	with open("Rakuten-Viki-Data//Video_attributes.csv", 'r') as f:
		next(f)
		for row in csv.reader(f):
			video_data.append(row)
		video_data = np.array(video_data)

	return behavior_data, user_data, cast_data, video_data

def preprocessData():
	# Load data files
	behavior_data, user_data, cast_data, video_data = loadDataFiles()

	# Prepare for converting
	# For video_id: remove prefix 'TV' and convert normally
	for row in behavior_data: 
		row[2] = row[2][2:]
	# For date_hour and mv_ratio: remove 
	behavior_data = np.delete(behavior_data, 3, 1)
	behavior_data = np.delete(behavior_data, 0, 1)

	# For country: remove prefix 'Country'
	for row in user_data:
		row[1] = row[1][7:]
	# For gender: remove because of too many missing values
	user_data = np.delete(user_data, 2, 1)	

	# For container_id: remove prefix "Container"
	# For person_id, remove prefix "Cast"
	for row in cast_data:
		row[0] = row[0][9:]
		row[1] = row[1][4:]
	# For country: count and index distinct values
	# Assign index value to corresponding country
	countries = cast_data[:, 2]
	countries = np.unique(countries)
	for row in cast_data:
		row[2] = np.nonzero(countries==row[2])[0][0]
	# For gender: treate 'f' as 0 and 'm' as 1
	cast_data[cast_data[0::, 3] == 'f', 3] = 0
	cast_data[cast_data[0::, 3] == 'm', 3] = 1

	# For video_id: remove prefix "TV"
	# For container_id: remove prefix "Container"
	# For content_owner_id: remove prefix "ContentOwner"
	for row in video_data:
		row[0] = row[0][2:]
		row[1] = row[1][9:]
		row[-3] = row[-3][12:]
	# For origin_country: count and index distinct values
	# Assign index value to corresponding country
	# Same as origin_language
	v_countries = video_data[:, 2]
	v_languages = video_data[:, 3]
	v_countries = np.unique(v_countries)
	v_languages = np.unique(v_languages)
	for row in video_data:
		row[2] = np.nonzero(v_countries==row[2])[0][0]
		row[3] = np.nonzero(v_languages==row[3])[0][0]
	# For adult: assign "False" as 0 and 'True' as 1
	video_data[video_data[0::, 4]=="False", 4] = 0
	video_data[video_data[0::, 4]=="True", 4] = 1
	# For broadcast_from and broadcast_to
	# apply the same as origin_country
	# and replace None value with randomized ones
	v_from = video_data[:, 5]
	v_from = np.unique(v_from[v_from!="None"])
	for row in video_data:
		if len(np.where(v_from==row[5])[0])!=0:
			row[5] = np.where(v_from==row[5])[0][0]  
	from_none = np.where(video_data[:, 5]=="None")
	video_data[from_none, 5] = np.random.randint(0, len(v_from))
	v_to = video_data[:, 6]
	v_to = np.unique(v_to[v_to!="None"])
	for row in video_data:
		if len(np.where(v_to==row[6])[0])!=0:
			row[6] = np.where(v_to==row[6])[0][0]  
	to_none = np.where(video_data[:, 6]=="None")
	video_data[to_none, 6] = np.random.randint(0, len(v_to))
	# For season number and gender: ignore for now
	video_data = np.delete(video_data, -4, 1)
	video_data = np.delete(video_data, -2, 1)

	# Convert strings to numbers
	behavior_data = behavior_data.astype(int)
	user_data = user_data.astype(int)
	cast_data = cast_data.astype(int)
	video_data = video_data.astype(int)

	# Join data together to create training data
	train = []

	for row in behavior_data:
		# Add user info
		train_row = user_data[np.searchsorted(user_data[:, 0], row[0])]
		# Add video info
		train_row = np.concatenate((train_row, video_data[np.searchsorted(video_data[:,0],row[1])]))
		# Add castor info
		#container_info = cast_data[np.searchsorted(cast_data[:, 0], train_row[3])][1:]
		#train_row = np.concatenate((train_row, container_info))
		# Add score (what we need to predict)
		train_row = np.concatenate((train_row, [row[2]]))
		train.append(train_row.tolist())
	train = np.array(train)

	return train, user_data, video_data

	# UNUSED
	# for row in behavior_data: 
	# 	# For date_hour: convert using datetime module
	# 	dt = datetime.strptime(row[0], "%Y-%m-%dT%H")
	# 	dtTuple = dt.timetuple()
	# 	row[0] = time.mktime(dtTuple)
	# 	row[0] = row[0][:-4]             # No need for minute represent

	# For gender: treat 'f' as 0 and 'm' as 1
	#user_data[user_data[0::, 2] == 'f', 2] = 0
	#user_data[user_data[0::, 2] == 'm', 2] = 1
	# Convert strings to numbers

def main():
	train, user, video = preprocessData()
	print(len(train))

if __name__ == "__main__":
	main()